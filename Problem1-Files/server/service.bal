import ballerina/http;
import ballerina/io;

# This is the studentKiosk service. 
# It is used to implement a service that creates, updates, and shows students from a record.

type student record {
    int student_id;
    string username;
    string lastname;
    string firstname;
    string[] prefered_formats;
    json[] past_subjects;
};
# The following can be used as a sample student:
# '{"student_id":101, "username":"Ed.exe", "lastname":"Zau", "firstname":"Edilson", "preferd_formats":["audio", "video", "text"], "past_subjects":{"Programming I": "A"}}'
student [] student_records = [];


service /studentKiosk on new http:Listener(9090) {

    resource function post addStudent(@http:Payload student new_student) returns json{
        io:println("Adding new student to records....");
        student_records.push(new_student);
        return {"Add":"Succesfully added " + new_student.firstname + " to the records."};
       
    }
    resource function get getStudents() returns student[] {
        io:println("Getting student records....");
        return student_records;
    }
}
