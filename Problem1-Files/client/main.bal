import ballerina/io;

type student record {
    int student_id;
    string username;
    string lastname;
    string firstname;
    string[] prefered_formats;
    json[] past_subjects;
};

public function main() returns error? {
   
    student sampleStudent = {
        student_id: 101,
        username: "ed.exe",
        lastname: "Zau",
        firstname: "Edilson",
        prefered_formats: [],
        past_subjects: []
    };
    io:print("");
}

