import ballerina/io;

functionManagmentClient ep = check new ("http://localhost:9090");

public function main() {
    Fn sampleFunction = {name:"Sample", parameters: ["time", "date"]};

    //making a rpc call
    string|error outcome = ep->add_new_fn(sampleFunction);
    io:println("Added to functions repository", outcome); 
}

