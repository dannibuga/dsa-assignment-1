import ballerina/grpc;
import ballerina/log;

listener grpc:Listener ep = new (9090);

Fn [] functionsList = [];

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "functionManagment" on ep {

    remote function add_new_fn(Fn value) returns string|error {
        log:printInfo(value.name);
        functionsList.push(value);
        return "Successfully added "+value.name.toString()+" function.";
    }
    remote function add_fns(Fn value) returns string|error {
        functionsList.push(value);
        return "Successfully added "+value.name.toString()+" function.";
    }
    remote function delete_fn(Fn value) returns string|error {
        //functionsList.remove(value);
        return "Successfully deleted "+value.name.toString()+" function.";
    }
    remote function show_fn(Fn value) returns string|error {
        return value.toString();
    }
    remote function show_all_fns(Fn value) returns string|error {
        return functionsList.toString();
    }
    remote function show_all_with_criteria(Fn value) returns string|error {
        return functionsList.toString();
    }
}

