import ballerina/grpc;

public isolated client class functionManagmentClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_new_fn(Fn|ContextFn req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        Fn message;
        if (req is ContextFn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManagment/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function add_new_fnContext(Fn|ContextFn req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        Fn message;
        if (req is ContextFn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManagment/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function add_fns(Fn|ContextFn req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        Fn message;
        if (req is ContextFn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManagment/add_fns", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function add_fnsContext(Fn|ContextFn req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        Fn message;
        if (req is ContextFn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManagment/add_fns", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function delete_fn(Fn|ContextFn req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        Fn message;
        if (req is ContextFn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManagment/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function delete_fnContext(Fn|ContextFn req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        Fn message;
        if (req is ContextFn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManagment/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function show_fn(Fn|ContextFn req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        Fn message;
        if (req is ContextFn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManagment/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function show_fnContext(Fn|ContextFn req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        Fn message;
        if (req is ContextFn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManagment/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function show_all_fns(Fn|ContextFn req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        Fn message;
        if (req is ContextFn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManagment/show_all_fns", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function show_all_fnsContext(Fn|ContextFn req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        Fn message;
        if (req is ContextFn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManagment/show_all_fns", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function show_all_with_criteria(Fn|ContextFn req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        Fn message;
        if (req is ContextFn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManagment/show_all_with_criteria", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function show_all_with_criteriaContext(Fn|ContextFn req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        Fn message;
        if (req is ContextFn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManagment/show_all_with_criteria", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }
}

public client class FunctionManagmentStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextString record {|
    string content;
    map<string|string[]> headers;
|};

public type ContextFn record {|
    Fn content;
    map<string|string[]> headers;
|};

public type response record {|
    string msg = "";
    Fn currentFuntion = {};
|};

public type Fn record {|
    string name = "";
    anydata[] parameters = [];
|};

const string ROOT_DESCRIPTOR = "0A0F696E746572666163652E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F1A19676F6F676C652F70726F746F6275662F616E792E70726F746F224E0A02466E12120A046E616D6518012001280952046E616D6512340A0A706172616D657465727318022003280B32142E676F6F676C652E70726F746F6275662E416E79520A706172616D657465727322490A08726573706F6E736512100A036D736718012001280952036D7367122B0A0E63757272656E7446756E74696F6E18022001280B32032E466E520E63757272656E7446756E74696F6E32C0020A1166756E6374696F6E4D616E61676D656E74122F0A0A6164645F6E65775F666E12032E466E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565122C0A076164645F666E7312032E466E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565122E0A0964656C6574655F666E12032E466E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565122C0A0773686F775F666E12032E466E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512310A0C73686F775F616C6C5F666E7312032E466E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565123B0A1673686F775F616C6C5F776974685F637269746572696112032E466E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"google/protobuf/any.proto": "0A19676F6F676C652F70726F746F6275662F616E792E70726F746F120F676F6F676C652E70726F746F62756622360A03416E7912190A08747970655F75726C18012001280952077479706555726C12140A0576616C756518022001280C520576616C7565424F0A13636F6D2E676F6F676C652E70726F746F6275664208416E7950726F746F50015A057479706573A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "interface.proto": "0A0F696E746572666163652E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F1A19676F6F676C652F70726F746F6275662F616E792E70726F746F224E0A02466E12120A046E616D6518012001280952046E616D6512340A0A706172616D657465727318022003280B32142E676F6F676C652E70726F746F6275662E416E79520A706172616D657465727322490A08726573706F6E736512100A036D736718012001280952036D7367122B0A0E63757272656E7446756E74696F6E18022001280B32032E466E520E63757272656E7446756E74696F6E32C0020A1166756E6374696F6E4D616E61676D656E74122F0A0A6164645F6E65775F666E12032E466E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565122C0A076164645F666E7312032E466E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565122E0A0964656C6574655F666E12032E466E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565122C0A0773686F775F666E12032E466E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512310A0C73686F775F616C6C5F666E7312032E466E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565123B0A1673686F775F616C6C5F776974685F637269746572696112032E466E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33"};
}

